CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    username VARCHAR UNIQUE NOT NULL,
    password VARCHAR NOT NULL,
    email VARCHAR UNIQUE,
    created_on TIMESTAMP
);

INSERT INTO users 
    (username, password)
    VALUES ('alex', 'temporar' );

CREATE TABLE books (
    id SERIAL PRIMARY KEY,
    isbn VARCHAR UNIQUE,
    title VARCHAR NOT NULL,
    author VARCHAR NOT NULL,
    year SMALLINT CHECK (year >= 0)
);

CREATE TABLE reviews (
    id SERIAL PRIMARY KEY,
    rating SMALLINT NOT NULL CHECK (rating > 0 AND rating < 6),
    review VARCHAR,
    user_id INTEGER REFERENCES users,
    book_id INTEGER REFERENCES books
);