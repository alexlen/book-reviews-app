import os
import sys
import requests


from flask import Flask, flash, jsonify, session, redirect, render_template, request, url_for
from flask_session import Session
from flask_bcrypt import Bcrypt
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

app = Flask(__name__)

# Check for environment variable
if not os.getenv("DATABASE_URL"):
    raise RuntimeError("DATABASE_URL is not set")

# Configure session to use filesystem
app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"
Session(app)

# Set up database
engine = create_engine(os.getenv("DATABASE_URL"))
db = scoped_session(sessionmaker(bind=engine))

# Expose hashing methods
bcrypt = Bcrypt(app)

@app.route("/")
def index():
    if 'user_id' in session:
        return redirect(url_for('search'))
    return render_template("index.html")

@app.route("/register", methods=["GET", "POST"])
def register():
    if 'user_id' in session:
        return render_template("error.html", message="Please log out first.")

    if request.method == "POST":
        # check if username already exists
        username = request.form.get("username") 
        if db.execute("SELECT * FROM users WHERE username = :username", {"username": username}).rowcount != 0:
            return render_template("register.html", message="That username already exists.")
        # hash password
        pw_hash = bcrypt.generate_password_hash(request.form.get("password")).decode('utf-8')
        # add user to database
        db.execute("INSERT INTO users (username, password) VALUES (:username, :password)", {"username": username, "password": pw_hash})
        db.commit()
        session['user_id'] = db.execute("SELECT id FROM users WHERE username = :username", 
                                        {"username": username}).fetchone()[0]
        return render_template("welcome.html")

    return render_template("register.html")

@app.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "POST":
        # check password submitted at login
        username = request.form.get("username") 
        password = request.form.get("password")
        pw_hash = db.execute("SELECT password FROM users WHERE username = :username", 
                             {"username": username}).fetchone()[0]
        if pw_hash is None or not bcrypt.check_password_hash(pw_hash, password):
            return render_template("login.html", message="Incorrect username or password.")
        # else we store the user id in the session
        session.clear()
        session['user_id'] = db.execute("SELECT id FROM users WHERE username = :username", 
                                        {"username": username}).fetchone()[0]
        flash('You have been logged in.')
    
    if 'user_id' in session:
        return redirect(url_for('search'))

    return render_template("login.html")

@app.route("/logout")
def logout():
    session.pop('user_id', None)
    flash('You have been logged out.') # flash success feedback on the next request
    return redirect(url_for('login'))

@app.route("/account", methods=["GET", "POST"])
def account():
    # check if user is logged in
    if 'user_id' not in session:
        return render_template("error.html", message="You must be logged in for this.")
    
    if request.method == "POST": # if account details have changed
        email = request.form.get("inputEmail")
        # check email is unique
        if db.execute("SELECT * FROM users WHERE email=:email", {"email": email}).rowcount != 0:
            flash('That email address is associated with another user. Is that your past self?')
        else:
            # add email to db
            db.execute("UPDATE users SET email=:email WHERE id=:id", {"email": email, "id": session['user_id']})
            db.commit()
            flash('Email address successfully saved.')
        
    user = db.execute("SELECT * FROM users WHERE id=:id", {"id": session['user_id']}).fetchone()

    return render_template("account.html", user=user)

@app.route("/search", methods=["GET", "POST"])
def search():
    # check if user is logged in
    if 'user_id' not in session:
        return render_template("error.html", message="You must be logged in to search books.")    
    if request.method == "POST": # if search form was used
        term = request.form.get("search").strip() # get rid of leading ot trailing whitespace
        # search database by partial match of title, author or isbn
        results = db.execute("SELECT * FROM books WHERE \
                              title ILIKE :term OR \
                              author ILIKE :term OR \
                              isbn ILIKE :term \
                              LIMIT 100", 
                             {"term": '%' + term + '%'}) 
        # if nothing found, say so
        if results.rowcount == 0: 
            return render_template("search.html", message="Your search terms did not match any results.")
        # else return results
        results = results.fetchall() 
        return render_template("search.html", results=results)
    return render_template("search.html")

@app.route("/book/<int:book_id>", methods=["GET", "POST"])
def book(book_id=None):
    # check if user is logged in
    if 'user_id' not in session:
        return render_template("error.html", message="You must be logged in to search books.")

    book = db.execute("SELECT isbn, title, author, year FROM books WHERE id = :book_id", {"book_id": book_id}).fetchone()
    # check if book exists
    if book == None:
        return render_template("error.html", message="Nothing is here."), 404

    # did user already submit a review?
    submitted_review = (db.execute("SELECT * FROM reviews WHERE user_id=:user_id AND book_id=:book_id", {"user_id": session['user_id'], "book_id": book_id}).rowcount != 0)

    if request.method == "POST": # if a review is being submitted
        # check that user has not already submitted a review for this book
        if submitted_review:
            return render_template("error.html", message="You have already submitted a review for this book."), 400
        try:
            rating = int(request.form.get("rating"))
            assert 0 < rating < 6
        except (TypeError, AssertionError):
            return render_template("error.html", message="Illegal rating value."), 400
        review = request.form.get("review")
        # add review to the reviews table in the database
        db.execute("INSERT INTO reviews (rating, review, user_id, book_id) \
                    VALUES (:rating, :review, :user_id, :book_id)", 
                   {"rating": rating, "review": review, "user_id": session['user_id'], "book_id": book_id})
        db.commit()
        submitted_review = True

    reviews = db.execute("SELECT rating, review, username FROM reviews JOIN users ON users.id=reviews.user_id WHERE book_id=:book_id", 
                         {"book_id": book_id}).fetchall()

    # get Goodreads stats: average rating and no of ratings
    res = requests.get("https://www.goodreads.com/book/review_counts.json", params={"key": "IV5QHIMyGbvGwJOE9sqrHA", "isbns": book.isbn})
    if res.status_code != 200:
        goodreads_stats = None
    else:
        goodreads_stats = res.json()

    return render_template("book.html", book=book, submitted_review=submitted_review, reviews=reviews, goodreads_stats=goodreads_stats)

@app.route("/api/<string:isbn>")
def book_api(isbn):
    """Return details about a single book"""

    book = db.execute("SELECT id, isbn, title, author, year FROM books WHERE isbn=:isbn", {"isbn": isbn}).fetchone()

    # check if book exists
    if book == None:
        return jsonify({"error": "Isbn not found"}), 404

    # get ratings
    reviews = db.execute("SELECT rating FROM reviews WHERE book_id=:book_id", {"book_id": book.id}).fetchall()
    review_count = len(reviews)

    # calculate avg rating
    ratings_sum = 0
    for review in reviews:
        ratings_sum += review.rating
    try:
        average_rating = ratings_sum/review_count 
    except ZeroDivisionError:
        average_rating = None

    return jsonify({
        "title": book.title,
        "author": book.author,
        "year": book.year,
        "isbn": book.isbn,
        "review_count": review_count,
        "average_rating": average_rating
    })
